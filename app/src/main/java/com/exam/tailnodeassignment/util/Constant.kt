package com.exam.tailnodeassignment.util

object Constant {
    final val USER_PHONE: String="user_phone"
    final val USER_NAME: String="user_name"
    final val PREFERENCE_NAME = "tailnoode"
}