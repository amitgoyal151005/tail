package com.exam.tailnodeassignment.util

import java.util.regex.Pattern

object Utils {
    public fun isValidMobile(phone: String): Boolean {
        if (phone == null || phone.length < 9 || phone.length > 13) {
            return false;
        } else {
            return android.util.Patterns.PHONE.matcher(phone).matches();
        }

    }
}