package com.exam.tailnodeassignment.view

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.exam.tailnodeassignment.util.Constant

class HomeViewModel (application: Application) : AndroidViewModel(application)  {
    private val isAlredyLogin: MutableLiveData<Boolean> = MutableLiveData()
    private val sharedPreferences: SharedPreferences =
        application.getSharedPreferences(Constant.PREFERENCE_NAME, Context.MODE_PRIVATE)

    fun isLogin():MutableLiveData<Boolean>{
        isAlredyLogin.postValue(!sharedPreferences.getString(Constant.USER_NAME,"")?.isEmpty()!!)
       return isAlredyLogin
    }


    fun saveValue(name: String, phoneNo: String) {
        sharedPreferences.edit()
            .putString(Constant.USER_NAME, name).apply()
        sharedPreferences.edit()
            .putString(Constant.USER_PHONE, phoneNo).apply()
        isAlredyLogin.postValue(true)
    }
}