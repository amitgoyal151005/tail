package com.exam.tailnodeassignment.view.getlocation

import android.Manifest
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.app.ActivityCompat

import androidx.activity.result.contract.ActivityResultContracts


import android.os.Build
import android.provider.Settings
import android.widget.Toast
import androidx.core.content.FileProvider
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import java.io.File
import android.os.StrictMode
import android.os.StrictMode.VmPolicy
import android.widget.Button
import com.exam.tailnodeassignment.R


class ShowLocationFragment : Fragment() {

    private val checkPermission =registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
        if (permissions[Manifest.permission.ACCESS_FINE_LOCATION] == true
            && permissions[Manifest.permission.ACCESS_FINE_LOCATION] == true
            && permissions[Manifest.permission.WRITE_EXTERNAL_STORAGE] == true
            && permissions[Manifest.permission.READ_EXTERNAL_STORAGE] == true
        ) {
            getLocation()
        } else {
            Toast.makeText(context, R.string.accept_permition, Toast.LENGTH_LONG).show()
        }
    }
    private val model: ShowLocationViewModel by activityViewModels()


    private lateinit var currentLocationTextView: TextView
    private lateinit var openFile: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val builder = VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())
        builder.detectFileUriExposure()
        return inflater.inflate(R.layout.fragment_show_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        currentLocationTextView = view.findViewById(R.id.current_location_tv);
        openFile=view.findViewById(R.id.location_file_btn);
    }

    override fun onStart() {
        super.onStart()
        openFile.setOnClickListener(View.OnClickListener {
            val file = File(context?.getExternalFilesDir(null),"myfile.txt")
           if (file.exists()) {
                val photoURI = FileProvider.getUriForFile(
                    requireContext(),
                    requireContext().applicationContext.packageName.toString() + ".provider",
                    file
                )
                val intent = Intent()
                intent.setAction(Intent.ACTION_VIEW)
                intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                intent.setDataAndType(photoURI, "text/plain");
                intent.putExtra(Intent.EXTRA_STREAM, photoURI)
                val j = Intent.createChooser(intent, getString(R.string.choose_application));
                startActivity(j);
            }
        })



        getLocation()
    }

    private fun getLocation()
    {
        if (isLocationEnabled() && checkPermissions()) {
            model.getLocation()
            model.liveLocation.observe(viewLifecycleOwner, Observer { value ->

                currentLocationTextView.text = value
            })
        }
    }


    @SuppressWarnings("deprecation")
    private fun isLocationEnabled(): Boolean {
        val isEnable = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
            // This is a new method provided in API 28
            val lm = requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
            lm.isLocationEnabled
        } else {
            // This was deprecated in API 28
            val mode: Int = Settings.Secure.getInt(
                requireContext().contentResolver, Settings.Secure.LOCATION_MODE,
                Settings.Secure.LOCATION_MODE_OFF
            )
            mode != Settings.Secure.LOCATION_MODE_OFF
        }
        if (!isEnable) {
            Toast.makeText(context, "Turn on location", Toast.LENGTH_LONG).show()
            val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(intent)
        }
        return isEnable
    }


    private fun checkPermissions(): Boolean {
        if (context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            } == PackageManager.PERMISSION_GRANTED &&
            context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.ACCESS_FINE_LOCATION
                )
            } == PackageManager.PERMISSION_GRANTED && context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                )
            } == PackageManager.PERMISSION_GRANTED&&
            context?.let {
                ActivityCompat.checkSelfPermission(
                    it,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE
                )
            } == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {

            checkPermission.launch(arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
            )
            )
        }

        return false
    }


}