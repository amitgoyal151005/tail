package com.exam.tailnodeassignment.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.lifecycle.Observer
import com.exam.tailnodeassignment.view.login.LoginFragment
import com.exam.tailnodeassignment.R
import com.exam.tailnodeassignment.view.getlocation.ShowLocationFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val model: HomeViewModel by viewModels()
        model.isLogin().observe(this, Observer { isLogin->
            if (isLogin)
            {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, ShowLocationFragment())
                    .commit()
            }
            else
            {
                supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.container, LoginFragment())
                    .commit()
            }

        })

    }
}