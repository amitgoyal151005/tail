package com.exam.tailnodeassignment.view.login

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.activityViewModels
import com.exam.tailnodeassignment.R
import com.exam.tailnodeassignment.databinding.FragmentLoginBinding
import com.exam.tailnodeassignment.util.Utils
import com.exam.tailnodeassignment.view.HomeViewModel


class LoginFragment : Fragment() {

    private lateinit var mBinding: FragmentLoginBinding
    private val model: HomeViewModel by activityViewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding.loginBtn.setOnClickListener(View.OnClickListener {
            if (isVaild()) {
                model.saveValue(mBinding.name.text.toString(),mBinding.phoneNo.text.toString())
            }
        })
    }

    private fun isVaild(): Boolean {
        var valid: Boolean = true
        if (mBinding.name.text.isEmpty()) {
            valid = false
            mBinding.nameInputLayout.error = "Enter a valid value"
        } else if (!Utils.isValidMobile(mBinding.phoneNo.text.toString())) {
            valid = false
            mBinding.phoneNoInputLayout.error = "Enter a valid value"
        }
        return valid;

    }

}