package com.exam.tailnodeassignment.view.getlocation

import android.Manifest
import android.app.Application
import android.content.Context
import android.content.pm.PackageManager
import android.location.Geocoder
import android.os.Looper
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import java.io.File

class ShowLocationViewModel(application: Application) : AndroidViewModel(application) {
    private val appContext: Context = application.applicationContext
    private lateinit var locationRequest: LocationRequest


    private val geocoder: Geocoder = Geocoder(application.applicationContext)
    val liveLocation: MutableLiveData<String> = MutableLiveData()

    private val locationCallback: LocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
            locationResult ?: return

            if (locationResult.locations.isNotEmpty()) {
                // get latest location
                val location =
                    locationResult.lastLocation
                val locationTxt =
                    geocoder.getFromLocation(location.latitude, location.longitude, 1)
                        .get(0).getAddressLine(0)

                appendInFile(locationTxt)
                liveLocation.postValue(locationTxt)
            }


        }
    }


    fun getLocation() {
        if (ActivityCompat.checkSelfPermission(
                appContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                appContext,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }
        try {
            locationRequest = LocationRequest.create()
            locationRequest.setInterval(5*60*1000)
                .fastestInterval =500

            locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
            LocationServices
                .getFusedLocationProviderClient(appContext).requestLocationUpdates(
                    locationRequest, locationCallback,
                    Looper.getMainLooper()
                )

            Log.e("", "")

        } catch (ex: Exception) {
            Log.e("", "")
        }

    }

    fun appendInFile(locationTxt: String) {

        try {
            val file = File(appContext.getExternalFilesDir(null),"myfile.txt")
            file.createNewFile()
            file.appendText("\n"+locationTxt)

            val contents = file.readText()
        }
        catch (e:java.lang.Exception)
        {
            Log.e("","")
        }

    }


}